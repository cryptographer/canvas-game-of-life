# canvas-game-of-life

This is an implementation of Conway's Game of Life in JavaScript, rendered using the canvas API. A demo is available [here](https://cryptographer.gitlab.io/canvas-game-of-life/).
