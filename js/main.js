import Conway from './conway.js';
import GUI from './lil-gui.esm.js';

const canvas = document.getElementById('canvas');
const context = canvas.getContext('2d', {alpha: false});

canvas.width = window.innerWidth;
canvas.height = window.innerHeight;

let conwayHeight = canvas.height;
let conwayWidth = canvas.width;
let cellSize = 8;

let conway = new Conway(conwayHeight, conwayWidth, cellSize);

let rAFID, elapsed;
let paused = false;
let previousTimestamp = window.performance.now();

let framerate = 30;
let frameInterval = 1000 / framerate;

conway.placeAcorn();

function togglePause() {
    if(paused) {
        previousTimestamp = window.performance.now();
        rAFID = window.requestAnimationFrame(animate);
    } else {
        conway.drawArray(context);
        conway.drawGrid(context);
        window.cancelAnimationFrame(rAFID);
    }

    paused = !paused;
}

function reset() {
    if(!paused) {
        togglePause();
    }

    conway.array = conway.initializeArray(conwayWidth, conwayHeight);
    conway.placeAcorn();

    conway.drawArray(context);
    conway.drawGrid(context);
}

canvas.addEventListener('mouseup', function (event) {
    let xPosition = Math.floor(event.offsetX / conway.cellSize);
    let yPosition = Math.floor(event.offsetY / conway.cellSize);
    conway.array[xPosition][yPosition] = conway.array[xPosition][yPosition] ? 0 : 1;

    conway.drawArray(context);
    conway.drawGrid(context);
});

window.addEventListener('keyup', (event) => {
    let key = event.key.toLowerCase();
    switch(key) {
        case ' ':
            togglePause();
            break;
        case 'r':
            reset();
            break;
    }
});

let guiControls = {
    pause: togglePause,
    reset: reset,
    framerate: framerate
};

const gui = new GUI({title: 'Conway Settings'});
gui.add(guiControls, 'pause').name('Pause (Space)');
gui.add(guiControls, 'reset').name('Reset (R)');
gui.add(guiControls, 'framerate', 1, 60, 1).name('Framerate').onChange(function() {
    frameInterval = 1000 / guiControls.framerate;
});

function animate(timestamp) {
    elapsed = timestamp - previousTimestamp;

    if(elapsed >= frameInterval) {
        conway.drawArray(context);
        conway.drawGrid(context);
        conway.updateArray();

        previousTimestamp = timestamp - (elapsed % frameInterval);
    }

    rAFID = window.requestAnimationFrame(animate);
}

animate();