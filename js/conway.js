export default class Conway {
    constructor(height, width, cellSize) {
        this.height = height;
        this.width = width;
        this.cellSize = cellSize;

        this.horizontalCells = Math.ceil(width / cellSize);
        this.verticalCells = Math.ceil(height / cellSize);

        this.neighborPositions = [
            [-1, -1], [0, -1], [1, -1],
            [-1, 0],           [1, 0],
            [-1, 1],  [0, 1],  [1, 1]
        ];

        this.array = this.initializeArray(this.horizontalCells, this.verticalCells);
    }

    initializeArray(xDim, yDim) {
        let array = new Array(xDim);

        for(let i = 0; i < xDim; i++) {
            array[i] = new Array(yDim);
            array[i].fill(0);
        }

        return array;
    }

    placeAcorn() {
        this.array[100][60] = 1;
        this.array[101][60] = 1;
        this.array[104][60] = 1;
        this.array[105][60] = 1;
        this.array[106][60] = 1;
        this.array[101][58] = 1;
        this.array[103][59] = 1;
    }

    getCellState(x, y) {
        if(x < 0 || y < 0 || x >= this.horizontalCells || y >= this.verticalCells) {
            return 0;
        } else {
            return this.array[x][y];
        }
    }

    getNeighborCount(x, y) {
        let neighborCount = 0;

        this.neighborPositions.forEach(offset => {
                if(this.getCellState(x + offset[0], y + offset[1])) {
                    neighborCount++;
                }
            }
        );

        return neighborCount;
    }

    updateArray() {
        let newArray = this.initializeArray(this.horizontalCells, this.verticalCells);

        for(let i = 0; i < this.horizontalCells; i++) {
            for(let j = 0; j < this.verticalCells; j++) {
                let neighborCount = this.getNeighborCount(i, j);

                if(this.array[i][j] === 1) {
                    if(neighborCount < 2 || neighborCount >3) {
                        newArray[i][j] = 0;
                    } else {
                        newArray[i][j] = 1;
                    }
                } else if(neighborCount === 3) {
                    newArray[i][j] = 1;
                }
            }
        }

        this.array = newArray;
    }

    drawArray(context) {
        for(let x = 0; x < this.horizontalCells; x++) {
            for(let y = 0; y < this.verticalCells; y++) {
                context.fillStyle = this.array[x][y] ? 'black' : 'white';
                context.fillRect(x * this.cellSize, y * this.cellSize, this.cellSize, this.cellSize);
            }
        }
    }

    drawGrid(context) {
        for(let i = 0; i < this.horizontalCells; i++) {
            context.beginPath();
            context.moveTo(i * this.cellSize, 0);
            context.lineTo(i * this.cellSize, this.height);
            context.stroke();
        }

        for(let i = 0; i < this.verticalCells; i++) {
            context.beginPath();
            context.moveTo(0, i * this.cellSize);
            context.lineTo(this.width, i * this.cellSize);
            context.stroke();
        }
    }
}